
<?php 
include('../functions.php');

if (!isAdmin()) {
	$_SESSION['msg'] = "You must log in first";
	header('location: ../login.php');
}

if (isset($_GET['logout'])) {
	session_destroy();
	unset($_SESSION['user']);
	header("location: ../login.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<title></title>

<style>
#header {
    /*background: #BBB;*/
     width: 100%;
    height: 40px;
}

#menu {
    height: 700px;  float:left;
    width: 25%;
    /*background:red;*/
}

#cont {

    width: 50%;
    text-align: center;
    font-size: 12px;
    font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif;
    height: 700px; float:left;
    margin: 0 auto;
    border: #000 1px solid;
}



#calchat {
    width: 24%;
    height: 700px; float:left;
}



#all {
    /*background: #cdf3cd;*/
    margin-left:0.25%;
    float:center;

}

#cal {

    height: 320px;
    margin: 5px;
    border: #c3e6c3 2px solid;
    padding: 10px 50px;
}

#chat {
    height: 320px;

    margin: 5px;
    border: #c3e6c3 2px solid;
    padding: 10px 50px;
}

form
    {
        text-align:center;
    }

#menu{
/*	height: 600px;
	float: left;
	background-color: #FFF;
	
   margin-left: auto;
   margin-right: auto;
  padding: 0px; 
   font-family: Sans;
   font-size : 30px;
   text-align: center;
   border: 1px solid #FFFFFF;
	border-radius: 40px 40px 40px 40px;*/
}

/* -- nav */
#nav-in ul{
	padding: 1px;
	list-style: none;
}

#nav-in li ul li{
	border-left: 2px solid brown;
}

#nav-in ul li{
	background-color: black;
	float: left;
	height:50px;
	width : 300px;
	line-height : 50px;
	opacity : .8;	
	text-align :center;
	font-size : 20px;
	font-family : Arial,DejaVuSans ;
	margin-right : 4px;
	position: relative;
    margin:4px;
}

#nav-in ul li a {
	text-decoration : none;
	color : white;
	display:block;
	border-bottom: 0px solid brown;

}
#nav-in ul li a:hover {
	background-color : red;
}

#nav-in ul li ul li {
	display : none;
}

#nav-in ul li:hover ul li {
	display : block;
}

#nav-out
{
     padding: 30px;
     text-align: center;
}

/* ---- nav */

<?php include("../cal/includehead.php") ?>
#footer {
    width: 100%;
    height: 30px; clear:both; text-align: center;
    background: #BBB;
}
</style>

</head>
<body>

<div id="all">

<div id="header">
    <div style="float:right">
    <img style="width:10px" src="../img/profile.png"  >
            <?php  if (isset($_SESSION['user'])) : ?>
                <strong><?php echo $_SESSION['user']['username']; ?></strong>
                <small>
                    <i  style="color: #888;">(<?php echo ucfirst($_SESSION['user']['user_type']); ?>)</i> 
                    <br>
                    <a href="../admin/home.php?logout='1'" style="color: red;">logout</a>
                       &nbsp; <a href="../admin/create_user.php"> + add user</a>
                </small>
            <?php endif ?>
    </div>
</div>


<div id="menu">
    <div id="content">
        <div id="nav-out">
            <div id="nav-in">
                <ul>
                    <li><a href="prof_commun.php">Espace commun</a></li>
                    <li><a href="../chatApp/minichat_post.php">Chat</a></li>
                    <li><a href="../cal/index.php">Agenda</a></li>
                    <li><a href="../AttendanceApp">Attendance</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

    <div id="cont"><?php include("contents.php"); ?></div>
        
    <div id="footer"><?php include("../inc/footer.php");?></div>
</div>

</body>
</html>
