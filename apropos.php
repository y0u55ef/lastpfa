<!DOCTYPE html>
<html>
<head>
	<title> Aperçu sur la formation</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style1.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js" integrity="sha384-u/bQvRA/1bobcXlcEYpsEdFVK/vJs3+T+nXLsBYJthmdBuavHvAW6UsmqO2Gd/F9" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">ENSIAS Ingénieurie e-logistique</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		  <span class="navbar-toggler-icon"></span>
		</button>
	  
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
		  <ul class="navbar-nav mr-auto">
			<li class="nav-item active">
			  <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
			</li>
			
			
			
		  </ul>
		</div>
	  </nav>
			  
		  </nav>

	<nav class="menu">
     <ul>
      <li><a href="#ln">Coordonnateur</a></li>    
      <li><a href="#lnn">Objectifs</a></li> 
      <li><a href="#lnnn">Compétences à acquérir </a></li>
      <li><a href="#lnnnn">Débouchés</a></li>
      <li><a href="#lnnm">La formation</a></li>
           </ul><br><br><br>
    </nav>
<h3 class="t1"> Présentation de la formation </h3>
<img src="img/Ensias4.jpg" >
<a name="ln"></a>
<h3>Coordonnateur</h3>

<p><strong>Coordonnateur :</strong>  Youssef BENADADA </br>
	<strong>E-mail :</strong>  benadada@ensias.ma -  yss.benadada@gmail.com </br>
	<strong>Effectif prévu :</strong>  30 </br>
<a name="lnn"></a>

<h3>Objectifs de la formation </h3>
<r> &nbsp; &nbsp; &nbsp; &nbsp;	Dans un contexte de globalisation et de forte concurrence, la maîtrise des techniques de transport et de la logistique est un facteur clé de succès pour les entreprises. La maîtrise de ces techniques permet à l’entreprise de mieux satisfaire les besoins de ses clients tout en garantissant la rentabilité des contrats.</br>
	&nbsp; &nbsp; &nbsp; &nbsp;	L’Etat marocain accorde depuis plusieurs années, au plus haut niveau, une importance particulière à la logistique en mettant en oeuvre une stratégie nationale du développement de la logistique. Ainsi, il y a eu la création de l’Agence Marocaine de Développement Logistique (AMDL) et puis l’Observatoire Marocain de la Compétitivité Logistique (OMCL) respectivement en 2010 et 2013. Enfin en 2014, des conventions d’application du contrat programme logistique ont été signées devant sa Majesté le Roi. Les prévisions de l’AMDL estiment les besoins nationaux en ingénieurs logisticiens à plus de 5000 à l’horizon de 2030.</br>
	&nbsp; &nbsp; &nbsp; &nbsp;	Encouragée par les bons résultats de l’expérience de la formation d’ingénieurs logisticiens au sein de l’option Ingénierie de Logistique lancée en 2010, l’équipe pédagogique a décidé de créer la filière Ingénierie e-Logistique à partir de l’année universitaire 2014-15. Cette formation devra contribuer à la réussite de la stratégie nationale pour le développement de la compétitivité logistique du Maroc qui vise à réduire le coût de la logistique et à améliorer ainsi la compétitivité des entreprises marocaines à l’échelle internationale. Notre équipe s’engage à continuer à former des ingénieurs de haut niveau dans le domaine de la logistique, spécialité émergente, avec l’espoir de répondre à une partie de la demande croissante du tissu économique national.</br>
</r>


<a name="lnnn"></a>
<h3>Compétences à acquérir </h3>
<r>&nbsp; &nbsp; &nbsp; &nbsp; Après un tronc commun avec les autres filières de l’ENSIAS de deux semestres dédié à l’acquisition de connaissances et de compétences solides indispensables à tout informaticien, l’élève ingénieur de la filière Ingénierie e-Logistique devra acquérir les compétences et les outils indispensables lui permettant de maîtriser la gestion d’une chaîne logistique afin d’accroître la rentabilité et le service rendu aux clients. Il pourra ainsi exercer des fonctions en entreprises tournées vers la conception et la gestion globale de systèmes logistiques. Ainsi, en plus d’acquérir les connaissances de base en informatique et en aide à la décision, le diplômé de la filière IeL devra être en mesure de :<br>
	&nbsp;- Découvrir, modéliser, résoudre, analyser un problème de la chaîne logistique<br>
	&nbsp;- Concevoir un système logistique d’une entreprise<br>
	&nbsp;- Gérer le système d’information logistique d’une entreprise <br>
	&nbsp;- Maîtriser la planification des tâches au sein d’une entreprise <br>
	&nbsp;- …<br>
</r>
<a name="lnnnn"></a>

<h3>Débouchés</h3> 
<r> &nbsp; &nbsp; &nbsp; &nbsp; Le diplômé de la filière Ingénierie e-Logistique (IeL) peut occuper le poste de responsable logisticien dans toute entreprise privée ou publique nécessitant des ingénieurs à la fois logisticiens et informaticiens ayant une réelle capacité de résoudre des problèmes décisionnels. Ainsi, le lauréat peut travailler dans de nombreux secteurs d’activité en relation avec :<br>
	&nbsp;- Les métiers du transport<br>
	&nbsp;- Les métiers de l'entreposage et la manutention<br>
    &nbsp;- Les métiers de la logistique de soutien<br>
    &nbsp;- Les métiers de la logistique de production : prévision, approvisionnement, production, stockage<br>
    &nbsp;- Les métiers de la logistique de distribution<br>
    &nbsp;- Les métiers des systèmes d’information.</r>

<a name="lnnm"></a>	
<h3>La formation</h3>
<o><strong> Semestre 1</strong></br></strong></o>
<r>Algorithmique & Programmation<br>
	Structures de données<br>
	Electronique numérique<br>
	Architecture des ordinateurs<br>
	Eléments de Recherche opérationnelle<br>
	Probabilité Appliquées<br>
	Gestion, Economie et Finance 1<br>
	Langue et communication 1<br><br>
</r>	
<o><strong> Semestre 2</strong></br></strong></o>	
<r>Bases de données Relationnelles<br>
Informatique théorique<br>
Réseaux de communication<br>
Système d’exploitation<br>
Programmation Orientée Objet<br>
Projet de filière<br>
Gestion, Economie et Finance 2<br>
Langue et communication 2<br><br>
</r>
<o><strong> Semestre 3</strong></br></strong></o>	
<r>Système d’information<br>
	Management industriel et logistique<br>
	Génie Logiciel Objet<br>
	Méthodes Numériques Avancées<br>
	Statistiques et Analyse de Données<br>
	Techniques d’optimisation<br>
	Culture Entrepreneuriale<br>
	Langues et communication 3<br><br>
</r>
<o><strong> Semestre 4</strong></br></strong></o>
<r>Chaîne logistique stochastique<br>
	Modélisation de la chaîne logistique<br>
	Système d’information logistique 1<br>
	Techniques avancées d’optimisation<br>
	Réseaux logistiques et entreposage<br>
	Projet de fin d’année<br>
	Droit et Management<br>
	Langue et communication 4<br><br>
</r>
<o><strong> Semestre 5</strong></br></strong></o>
<r>Atelier de Modélisation<br>
	Projet fédérateur<br>
	Simulation de la chaîne logistique<br>
	Système d’information logistique 2<br>
	Systèmes logistiques prédictifs<br>
	E-Logistique<br>
	Amélioration des performances de la chaîne logistique<br>
	Anglais et stage de 2A<br><br>
</r>
<o><strong> Semestre 6</strong></br></strong></o>
<r>Projet de Fin d’Etude<br>
</r>





</body>
</html>
